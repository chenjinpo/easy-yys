import utils,random,time,subprocess,cv2

upleft = (0, 0)
downright = (1136, 700)
a,b = upleft
c,d = downright
monitor = {"top": b, "left": a, "width": c, "height": d}

def initParams(imgs1,monitor1):
    global imgs,monitor
    imgs=imgs1
    monitor=monitor1

#启动阴阳师
def openYYS():

    app_path_with_params = [
        r"D:\Program Files\Netease\MuMuPlayer-12.0\shell\MuMuPlayer.exe",
        "-p",
        "com.netease.onmyoji.wyzymnqsd_cps",
        "-v",
        "0"
    ]
    subprocess.Popen(app_path_with_params)

#点击进入游戏
def attendGame():
    x=random.randint(1,5)
    y=random.randint(1,5)
    utils.touch((560+x,535+y))

#御魂司机
def yuhun():
    global imgs,monitor
    last_click=''
    cishu=0
    refresh=0
    while True :
        #鼠标移到最右侧中止    

        #截屏
        screen=utils.screenshot(utils.adb_path, monitor)
        
        #print('screen shot ok',time.ctime())
        #体力不足
        want = imgs['notili']
        size = want[0].shape
        h, w , ___ = size
        target = screen
        pts = utils.locate(target,want,0)
        if not len(pts) == 0:
            print('体力不足')
            return {'staus':'error','msg':'体力不足'}

        #自动点击通关结束后的页面
        for i in ['jujue','tiaozhan','tiaozhan2',\
                  'moren','queding','querenyuhun','ying',\
                  'jiangli','jiangli2',\
                  'jixu','shibai']:
            want = imgs[i]
            size = want[0].shape
            h, w , ___ = size
            target = screen
            pts = utils.locate(target,want,0)
            if not len(pts) == 0:
                if last_click==i:
                    refresh=refresh+1
                elif i=='querenyuhun':
                    refresh=refresh+2
                else:
                    refresh=0
                last_click=i
                #print('重复次数：',refresh)
                if refresh>6 or cishu>200:
                    print('进攻次数上限')
                    return {'staus':'end','msg':'进攻次数上限'}
                
                if i == 'tiaozhan' or i=='tiaozhan2':
                    if refresh==0:
                        cishu=cishu+1
                    print('挑战次数：',cishu)
                    t = random.randint(500,750) / 100
                else:
                    print('挑战中。。。',i)
                    t = random.randint(50,100) / 100
                xy = utils.cheat(pts[0], w, h-10 )
                utils.touch(xy)
                time.sleep(t)
                break

if __name__=='__main__':
    attendGame()
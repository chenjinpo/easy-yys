import cv2,time,os,random,sys,mss,copy,subprocess,pyautogui
import numpy
from PIL import ImageGrab

adb_path="D:\\Program Files\\Netease\\MuMuPlayer-12.0\\shell\\adb.exe"

def connectAdb():
    comm=[adb_path,'connect','127.0.0.1:16384']
    out1=subprocess.run(comm,shell=False,capture_output=True,check=False)
    print(out1.stdout.decode('utf-8'))

#点击
def touch(pos):
    x, y = pos
    comm=[adb_path,"shell","input","tap",str(x),str(y)]
    subprocess.run(comm,shell=False)

#截屏
def screenshot():
    comm=[adb_path,"shell","screencap","-p"]
    #隐藏终端窗口
    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    startupinfo.wShowWindow = subprocess.SW_HIDE
    creationflags = subprocess.CREATE_NO_WINDOW
    invisibledict = {
        "startupinfo": startupinfo,
        "creationflags": creationflags,
        "start_new_session": True,
    }
    image_bytes = subprocess.run(comm,shell=False,stdout=subprocess.PIPE,stderr=subprocess.PIPE,**invisibledict)
    image_bytes = image_bytes.stdout
    image_bytes = image_bytes.replace(b'\r\n', b'\n')
    screen = cv2.imdecode(numpy.frombuffer(image_bytes, numpy.uint8),cv2.IMREAD_COLOR)
    
    return screen

def openApp(app_path_with_params):
    subprocess.Popen(app_path_with_params)

def locate(backend,want):
    loc_pos=[]
    
    result=cv2.matchTemplate(backend,want,cv2.TM_CCOEFF_NORMED)
    location=numpy.where(result>=0.95)

    h,w=want.shape[:-1] #want.shape[:-1]

    n,ex,ey=1,0,0
    for pt in zip(*location[::-1]):    #其实这里经常是空的
        x,y=pt[0]+int(w/2),pt[1]+int(h/2)
        if (x-ex)+(y-ey)<15:  #去掉邻近重复的点
            continue
        ex,ey=x,y
        x,y=int(x),int(y)
            
        loc_pos.append([x,y])

    if len(loc_pos)==0:
        pass

    return loc_pos

def load_imgs(path):   
    mubiao = {}
    
    file_list = os.listdir(path)
    for file in file_list:
        name = file.split('.')[0]
        file_path = path + '/' + file
        a = cv2.imread(file_path) 
        mubiao[name] = a

    return mubiao
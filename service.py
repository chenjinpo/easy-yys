import api,os

def initConnect():
    api.connectAdb()

#读取全部目标图片
def initWantPics():
    global wantPics
    path = os.getcwd() + '/png'
    wantPics = api.load_imgs(path)
    
#打开阴阳师mumu
def openYYS():
    app_path_with_params = [
        r"D:\Program Files\Netease\MuMuPlayer-12.0\shell\MuMuPlayer.exe",
        "-p",
        "com.netease.onmyoji.wyzymnqsd_cps",
        "-v",
        "0"
    ]
    
    api.openApp(app_path_with_params)
   
    

#检测当前是否是登录页面
def isLoginPage():
    global wantPics
    img = api.screenshot()
    print(api.locate(img,wantPics['loginpage']))

import sys,subprocess,time,datetime
import utils,action



#每日启动阴阳师程序
#action.openYYS()
#time.sleep(30)


#获取adb地址
adb_path=utils.getAdbPath()
#输出连接信息
print(utils.getConnetInfo(adb_path))

#检测及调整分辨率
if not utils.checkScSize(adb_path,640,1136):
    print("开始调整分辨率")
    utils.changeSceenSize(adb_path,640,1136)
    if utils.checkScSize(adb_path,640,1136):
        print("分辨率调整成功")
    else:
        print("分辨率调整失败")
        sys.exit(0)

# 读取文件 精度控制   显示名字
imgs = utils.load_imgs()

start_time = time.time()
#截屏，并裁剪以加速
upleft = (0, 0)
downright = (1136, 700)
a,b = upleft
c,d = downright
monitor = {"top": b, "left": a, "width": c, "height": d}
start = time.time()

action.initParams(imgs,monitor)

#constants
last_click=None

end = time.time()
hours, rem = divmod(end-start, 3600)
minutes, seconds = divmod(rem, 60)
print("运行时间：{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
print (datetime.datetime.now())


#进入游戏
action.attendGame()

#御魂司机
action.yuhun()

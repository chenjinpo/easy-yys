import cv2,time,os,random,sys,mss,copy,subprocess,pyautogui
import numpy
from PIL import ImageGrab

adb_path="D:\\Program Files\\Netease\\MuMuPlayer-12.0\\shell\\adb.exe"

def getAdbPath():
    print('操作系统:', sys.platform)
    print('开始检测模拟器'+"*"*20)
    #测试连接mumu12
    mumu_path=adb_path
    comm1=[mumu_path,'connect','127.0.0.1:16384']
    out1=subprocess.run(comm1,shell=False,capture_output=True,check=False)
    print(out1.stdout.decode('utf-8'))
    return mumu_path

def getConnetInfo(adb_path):
    #连接mumu12
    comm2=[adb_path,'devices']
    out2 = subprocess.run(comm2,shell=False,capture_output=True,check=False)
    return out2.stdout.decode('utf-8')
   

def screenshot(adb_path,monitor):
    comm=[adb_path,"shell","screencap","-p"]
    #隐藏终端窗口
    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    startupinfo.wShowWindow = subprocess.SW_HIDE
    creationflags = subprocess.CREATE_NO_WINDOW
    invisibledict = {
        "startupinfo": startupinfo,
        "creationflags": creationflags,
        "start_new_session": True,
    }
    image_bytes = subprocess.run(comm,shell=False,stdout=subprocess.PIPE,stderr=subprocess.PIPE,**invisibledict)
    image_bytes = image_bytes.stdout
    image_bytes = image_bytes.replace(b'\r\n', b'\n')
    screen = cv2.imdecode(numpy.frombuffer(image_bytes, numpy.uint8),cv2.IMREAD_COLOR)
    
    return screen



def checkScSize(adb_path,w1,h1):
    #截图
    screen=screenshot(adb_path,[])
    w=screen.shape[0]
    h=screen.shape[1]
    print('原始分辨率：',w,'x',h)
    if (w==w1 and h==h1) or (h==w1 and w==h1):
        print('无需修改分辨率')
        return True
    else:        
        print('需要调整分辨率')
        return False
    


def changeSceenSize(adb_path,w,h):
    if w>h:
        comm=[adb_path,"shell","wm","size","1136x640"]
        subprocess.run(comm,shell=False)
    elif w<=h:
        comm=[adb_path,"shell","wm","size","640x1136"]
        subprocess.run(comm,shell=False)

#按【文件内容，匹配精度，名称】格式批量聚聚要查找的目标图片，精度统一为0.95，名称为文件名
def load_imgs():   
    mubiao = {}
    
    path = os.getcwd() + '/png'
    
    file_list = os.listdir(path)
    for file in file_list:
        name = file.split('.')[0]
        file_path = path + '/' + file
        a = [ cv2.imread(file_path) , 0.95, name]
        mubiao[name] = a

    return mubiao

#在背景查找目标图片，并返回查找到的结果坐标列表，target是背景，want是要找目标
def locate(target,want, show=bool(0), msg=bool(0)):
    loc_pos=[]
    want,treshold,c_name=want[0],want[1],want[2]
    result=cv2.matchTemplate(target,want,cv2.TM_CCOEFF_NORMED)
    location=numpy.where(result>=treshold)

    h,w=want.shape[:-1] #want.shape[:-1]

    n,ex,ey=1,0,0
    for pt in zip(*location[::-1]):    #其实这里经常是空的
        x,y=pt[0]+int(w/2),pt[1]+int(h/2)
        if (x-ex)+(y-ey)<15:  #去掉邻近重复的点
            continue
        ex,ey=x,y
        x,y=int(x),int(y)
            
        loc_pos.append([x,y])

    if len(loc_pos)==0:
        pass

    return loc_pos

#随机偏移坐标，防止游戏的外挂检测。p是原坐标，w、n是目标图像宽高，返回目标范围内的一个随机坐标
def cheat(p, w, h):
    a,b = p
    w, h = int(w/3), int(h/3)
    if h<0:
        h=1
    c,d = random.randint(-w, w),random.randint(-h, h)
    e,f = a + c, b + d
    y = [e, f]
    return(y)


# 点击屏幕，参数pos为目标坐标
def touch(pos):
    global adb_enable
    x, y = pos
    comm=[adb_path,"shell","input","tap",str(x),str(y)]
    subprocess.run(comm,shell=False)

